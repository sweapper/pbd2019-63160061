package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import si.uni_lj.fri.pbd2019.runsup.ActiveWorkoutMapActivity;
import si.uni_lj.fri.pbd2019.runsup.MainActivity;
import si.uni_lj.fri.pbd2019.runsup.R;

// od activityja
import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.stetho.Stetho;
import com.google.android.gms.tasks.OnSuccessListener;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import si.uni_lj.fri.pbd2019.runsup.StopwatchActivity;
import si.uni_lj.fri.pbd2019.runsup.WorkoutDetailActivity;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;

public class StopwatchFragment extends Fragment {

    //DATABASE
    DatabaseHelper dbHelper;
    Dao<Workout,Long> workoutDao;

    private View v;
    private Button selectSport;
    private Button startButton;
    private Button endWorkout;
    private Button activeWorkout;
    private TextView dur;
    private TextView dist;
    private TextView cal;
    private TextView pac;
//    private OnFragmentInteractionListener mListener;
    // spremenljivke stopwatch activityja
    private BroadcastReceiver mBroadcastReceiver;
    static final int REQUEST_ID_LOCATION_PERMISSIONS = 0;

    static final int STATE_STOPPED = 0;
    static final int STATE_RUNNING = 1;
    static final int STATE_PAUSED = 2;
    static final int STATE_CONTINUE = 3;

    private int noLocChangeCounter = 0;

    private int sessionNumber = 0;
    private float speed;
    private long workoutId;
    private String userId;

    private int state;
    private int sportActivity = 0;
    private long duration = 0;
    private double distance = 0.0;
    private double calories = 0.0;
    private double pace = 0.0;
    private ArrayList<List<Location>> finalPositionList = new ArrayList<List<Location>>();
    private ArrayList<Location> positionList = new ArrayList<Location>();
    private Location lastLocation = null;

    public StopwatchFragment() {
        // Required empty public constructor
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().setTitle("RunsUp!");
        initDB();

        mBroadcastReceiver = new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                state = intent.getIntExtra("state", -1);
                if (state == 2) {
                    finalPositionList.add(positionList);
                    positionList = new ArrayList<Location>();
                } else {
                    pace = intent.getDoubleExtra("pace", -1);
                    duration = intent.getLongExtra("duration", -1);
                    distance = intent.getDoubleExtra("distance", -1);
                    calories = intent.getDoubleExtra("calories", -1);
                    speed = intent.getFloatExtra("speed", -1);

                    Bundle bun = intent.getExtras();
                    // ce je nova lokacija jo dodamo v list
                    if (bun != null) {
                        Location loc = (Location)bun.get("location");
                        // ce nu null se posodobi lokacija
                        if (loc != null) {
                            positionList.add(loc);
                            lastLocation = loc;
                            noLocChangeCounter = 0;
                            //novGpsPoint(loc);
                        } else {
                            noLocChangeCounter++;
                            if (noLocChangeCounter >= 10) {
                                noLocChangeCounter = 0;
                                //novGpsPoint(lastLocation);
                            }
                        }
                    }

                    dur.setText(MainHelper.formatDuration(duration/1000));
                    dist.setText(MainHelper.formatDistance(distance));
                    cal.setText(MainHelper.formatCalories(calories));
                    pac.setText(MainHelper.formatPace(pace));
                }
            }
        };

        reqLocation();

        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        menu.findItem(R.id.action_clear_history).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void initDB() {
        dbHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        try {
            workoutDao = dbHelper.workoutDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        backOnTrack();
    }

    public void backOnTrack () {
        SharedPreferences prefs = getActivity().getSharedPreferences(
                "runsup", Context.MODE_PRIVATE);

        if (prefs.getBoolean("startedWorkout", false)) {
            Log.d("kurko", "nadaljujem workout");
            List<Workout> lastWorkout = new ArrayList<>();
            try {
                lastWorkout = workoutDao.queryForAll();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            Workout ww;
            if (lastWorkout.size() > 0) {
                ww = lastWorkout.get(lastWorkout.size() - 1);
                workoutId = ww.getId();
                duration = ww.getDuration();
                distance = ww.getDistance();
                calories = ww.getTotalCalories();
                pace = ww.getPaceAvg();
                state = ww.getStatus();
                Log.d("kurko", "st workouta: " + String.valueOf(ww.getId()));
                dur.setText(MainHelper.formatDuration(duration/1000));
                dist.setText(MainHelper.formatDistance(distance));
                cal.setText(MainHelper.formatCalories(calories));
                pac.setText(MainHelper.formatPace(pace));
            }
            selectSport.setEnabled(false);

            if (state == STATE_RUNNING) {
                startButton.setText(R.string.stopwatch_stop);
                endWorkout.setVisibility(View.GONE);
            } else if (state == STATE_CONTINUE) {
                startButton.setText(R.string.stopwatch_stop);
                endWorkout.setVisibility(View.GONE);
            } else if (state == STATE_PAUSED) {
                endWorkout.setVisibility(View.VISIBLE);
                startButton.setText(R.string.stopwatch_continue);
            } else if (state == STATE_STOPPED) {
                endWorkout.setVisibility(View.VISIBLE);
                startButton.setText(R.string.stopwatch_continue);
            }
            Intent intent = new	Intent(getActivity(), TrackerService.class);
            getActivity().startService(intent);
        } else {
            dur.setText(MainHelper.formatDuration(0));
            dist.setText(MainHelper.formatDistance(0));
            cal.setText(MainHelper.formatCalories(0));
            pac.setText(MainHelper.formatPace(0));
            startButton.setText(R.string.stopwatch_start);
            endWorkout.setVisibility(View.GONE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_stopwatch, container, false);
        dur = (TextView) v.findViewById(R.id.textview_stopwatch_duration);
        dist = (TextView) v.findViewById(R.id.textview_stopwatch_distance);
        cal = (TextView) v.findViewById(R.id.textview_stopwatch_calories);
        pac = (TextView) v.findViewById(R.id.textview_stopwatch_pace);
        dodajOnClickListenerje(v);
        return v;
    }

    public void dodajOnClickListenerje(View view) {
        selectSport = (Button) view.findViewById(R.id.button_stopwatch_selectsport);
        selectSport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
                builderSingle.setIcon(R.drawable.ic_directions_run_black_24dp);
                builderSingle.setTitle("Select Sport Activity");

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_singlechoice);
                for (int activity = 0; activity < 3; activity++) {
                    arrayAdapter.add(SportActivities.getSportActivityName(activity));
                }
                builderSingle.setNegativeButton(getResources().getString(R.string.cancel), null);
                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override public void onClick(DialogInterface dialog, int which) {
                        changeSportActivity(which);
                        Intent intent = new	Intent(getActivity(), TrackerService.class);
                        intent.putExtra("sportActivity", sportActivity);
                        intent.setAction("si.uni_lj.fri.pdb2019.runsup.UPDATE_SPORT_ACTIVITY");
                        getActivity().startService(intent);
                    }
                });
                builderSingle.show();
            }
        });

        startButton = (Button) view.findViewById(R.id.button_stopwatch_start);
        endWorkout = (Button) view.findViewById(R.id.button_stopwatch_endworkout);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textStart = startButton.getText().toString();
                if (textStart.equals("Start")) {
                    sessionNumber = 0;
                    reqLocation();
                    selectSport.setEnabled(false);
                    startButton.setText(R.string.stopwatch_stop);
                    List<Workout> lastWorkout = new ArrayList<>();
                    try {
                        lastWorkout = workoutDao.queryForAll();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    if (lastWorkout.size() == 0) {
                        workoutId = 1;
                    } else {
                        workoutId = lastWorkout.get(lastWorkout.size()-1).getId();
                    }
                    Intent intent = new	Intent(getActivity(), TrackerService.class);
                    intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_START");
                    SharedPreferences prefs = getActivity().getSharedPreferences(
                            "runsup", Context.MODE_PRIVATE);
                    Boolean jePrijavljen = prefs.getBoolean("userSignedIn", false);
                    if (jePrijavljen) {
                        userId = prefs.getString("userId", "");
                        intent.putExtra("accId", userId);
                    } else {
                        intent.putExtra("accId", "");
                    }
                    intent.putExtra("praviId", workoutId);
                    getActivity().startService(intent);
                } else if (textStart.equals("Stop")) {
                    startButton.setText(R.string.stopwatch_continue);
                    endWorkout.setVisibility(View.VISIBLE);
                    Intent intent = new	Intent(getActivity(), TrackerService.class);
                    intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE");
                    getActivity().startService(intent);
                } else if (textStart.equals("Continue")) {
                    reqLocation();
                    startButton.setText(R.string.stopwatch_stop);
                    endWorkout.setVisibility(View.GONE);
                    sessionNumber++;
                    Intent intent = new	Intent(getActivity(), TrackerService.class);
                    intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_CONTINUE");
                    getActivity().startService(intent);
                }
            }
        });

        endWorkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.stopwatch_endworkout)
                        .setIcon(R.drawable.ic_warning_black_24dp)
                        .setMessage(R.string.end)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int a) {
                                selectSport.setEnabled(true);

                                Intent intent2 = new Intent(getActivity(), TrackerService.class);
                                intent2.setAction("si.uni_lj.fri.pdb2019.runsup.KILL");
                                getActivity().startService(intent2);

                                Intent intent = new Intent(getActivity(), WorkoutDetailActivity.class);
                                intent.putExtra("sportActivity", sportActivity);
                                intent.putExtra("duration", duration);
                                intent.putExtra("distance", distance);
                                intent.putExtra("calories", calories);
                                intent.putExtra("workoutId", workoutId);
                                intent.putExtra("pace", pace);
                                intent.putExtra("finalPositionList", finalPositionList);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton(R.string.no, null)
                        .show();

                Intent intent = new	Intent(getActivity(), TrackerService.class);
                intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_STOP");
                getActivity().startService(intent);
            }
        });

        activeWorkout = (Button) view.findViewById(R.id.button_stopwatch_activeworkout);
        activeWorkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new	Intent(getActivity(), ActiveWorkoutMapActivity.class);
                intent.putExtra("workoutId", workoutId);
                startActivity(intent);
            }
        });
    }

    public void askLocationPermission() {
        Snackbar.make(getActivity().findViewById(R.id.drawer_layout), R.string.location,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                        Manifest.permission.ACCESS_FINE_LOCATION},
                                REQUEST_ID_LOCATION_PERMISSIONS);
                    }
                })
                .show();
    }

    public void reqLocation() {
        SharedPreferences prefs = getActivity().getSharedPreferences(
                "runsup", Context.MODE_PRIVATE);
        if (prefs.getBoolean("locationEnabled", true)) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_ID_LOCATION_PERMISSIONS);
        }
    }

    public void changeSportActivity (int activity) {
        this.sportActivity = activity;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (state == STATE_STOPPED) {
            Intent intent = new	Intent(getActivity(), TrackerService.class);
            getActivity().startService(intent);
        }
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(mBroadcastReceiver, new IntentFilter("si.uni_lj.fri.pbd2019.runsup.TICK"));
        Log.d("kurko", "resumed");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (state == STATE_STOPPED) {
            Intent intent = new	Intent(getActivity(), TrackerService.class);
            getActivity().stopService(intent);
        }
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mBroadcastReceiver);
        Log.d("kurko", "paused");
    }
}
