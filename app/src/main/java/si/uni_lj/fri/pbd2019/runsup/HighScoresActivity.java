package si.uni_lj.fri.pbd2019.runsup;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.Advanture;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class HighScoresActivity extends AppCompatActivity {

    //DATABASE
    DatabaseHelper dbHelper;
    Dao<Advanture,Long> advantureDao;

    List<Advanture> allAdvantures = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_scores);

        initDB();

        try {
            QueryBuilder<Advanture, Long> queryBuilder = advantureDao.queryBuilder();
            queryBuilder.orderBy("score", false);
            queryBuilder.limit(15L);
            allAdvantures = queryBuilder.query();
        } catch (Exception e) {
            e.printStackTrace();
        }

        ListView mListView = (ListView) findViewById(R.id.listview_high_scores);

        if (allAdvantures == null || allAdvantures.size() == 0) {
            Toast.makeText(HighScoresActivity.this, "Something went wrong, please try again", Toast.LENGTH_LONG).show();
        } else {
            int position = 1;
            for (Advanture singleAdvanture : allAdvantures) {
                singleAdvanture.setPosition(position++);
            }
            HighScoreAdapter adapter = new HighScoreAdapter(HighScoresActivity.this, R.layout.adapter_high_scores, allAdvantures);
            mListView.setAdapter(adapter);
        }
    }

    private void initDB () {
        dbHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try {
            advantureDao = dbHelper.advantureDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
