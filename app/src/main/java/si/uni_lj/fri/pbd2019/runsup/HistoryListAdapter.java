package si.uni_lj.fri.pbd2019.runsup;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;

public class HistoryListAdapter extends ArrayAdapter<Workout> {

    private Context mContext;
    private int mResource;

    public HistoryListAdapter(@NonNull Context context, int resource, List<Workout> allWorkouts) {
        super(context, resource, allWorkouts);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String title = getItem(position).getTitle();
        Date date = getItem(position).getLastUpdate();
        Long duration = getItem(position).getDuration();
        int sportActivity = getItem(position).getSportActivity();
        Double distance = getItem(position).getDistance();
        Double calories = getItem(position).getTotalCalories();
        Double pace = getItem(position).getPaceAvg();


        DateFormat konecWorkouta = SimpleDateFormat.getDateTimeInstance();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView tvTitle = (TextView) convertView.findViewById(R.id.textview_history_title);
        TextView workoutDate = (TextView) convertView.findViewById(R.id.textview_history_datetime);
        TextView tvSportActivity = (TextView) convertView.findViewById(R.id.textview_history_sportactivity);

        //TODO sprobaj ali je milja ali km; in cse unite dej iz resourca

        tvTitle.setText(title);
        workoutDate.setText(konecWorkouta.format(date));
        String rezultat = MainHelper.formatDuration(duration/1000) + " " +
                MainHelper.formatActivity(sportActivity) + " | " +
                MainHelper.formatDistance(distance) + " km | " +
                MainHelper.formatCalories(calories) + " kcal | avg " +
                MainHelper.formatPace(pace) + "min/km";
        tvSportActivity.setText(rezultat);

        return convertView;
    }
    //setContentView(R.layout.adapter_history);
}
