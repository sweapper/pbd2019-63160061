package si.uni_lj.fri.pbd2019.runsup.helpers;

public final class MainHelper {
    private static final float MpS_TO_MIpH = 2.23694f;
    private static final float KM_TO_MI = 0.62137119223734f;
    private static final float MINpKM_TO_MINpMI = 1.609344f;

    // dobim sekunde in returnam HH:MM:SS
    public static String formatDuration(long time){
        int ure = (int) time / 3600;
        time -= ure * 3600;
        int minute = (int) time / 60;
        time -= minute * 60;
        String ureS;
        String minuteS;
        String sekundeS;
        if (ure < 10) {
            ureS = "0" + Integer.toString(ure);
        } else {
            ureS = Integer.toString(ure);
        }
        if (minute < 10) {
            minuteS = "0" + Integer.toString(minute);
        } else {
            minuteS = Integer.toString(minute);
        }
        if (time < 10) {
            sekundeS = "0" + Long.toString(time);
        } else {
            sekundeS = Long.toString(time);
        }
        return ureS + ":" + minuteS + ":" + sekundeS;
    }

    //TODO a zaokrozm k integer al na gor?
    // dobim metre, vrnem km na 2 decimalki
    public static String formatDistance(double n) {
        return String.format("%.2f", n/1000.0);
    }

    public static String formatActivity (int n) {
        switch (n) {
            case (0):
                return "Running";
            case (1):
                return "Walking";
            case (2):
                return "Cycling";
        }
        return "Running";
    }

    // zaokrožiš na 2 decimalke
    public static String formatPace(double n) {
        return String.format("%.2f", n);
    }

    // zaokrozi na int value
    public static String formatCalories(double n){
        int k = (int) Math.round(n);
        return Integer.toString(k);
    }

    public static double kmToMi(double n) {
        return n * KM_TO_MI;
    }

    public static double mpsToMiph(double n) {
        return n * MpS_TO_MIpH;
    }

    public static double minpkmToMinpmi(double n) {
        return n * MINpKM_TO_MINpMI;
    }

    public static double kmphToMiph(double n) { return n * KM_TO_MI;}

}
