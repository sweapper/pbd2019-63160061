package si.uni_lj.fri.pbd2019.runsup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.DrawableRes;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.stetho.inspector.protocol.module.Database;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.Advanture;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.services.AdvantureService;

public class AdvantureMapActivity extends FragmentActivity implements OnMapReadyCallback {

    // Broadcast
    private BroadcastReceiver mBroadcastReceiver;

    //DATABASE
    DatabaseHelper dbHelper;
    Dao<Advanture,Long> advantureDao;
    private int counter = -1;
    static final int REQUEST_ID_LOCATION_PERMISSIONS = 0;

    private GoogleMap mMap;
    private Polyline polyLine;
    Marker endMarker;
    List<Location> checkpointLocations;
    List<Marker> checkpointMarkers;
    private ArrayList<Location> positionList = new ArrayList<>();

    private String playerName = "Player 1";
    MediaPlayer checkpointReachedSound;
    MediaPlayer gameOverSound;
    private Vibrator vibrator;
    private long[] checkpointReachedPattern = {100, 800};
    Marker mChaserMarker1;
    Marker mChaserMarker2;
    boolean chaser1close = false;
    boolean chaser2close = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advanture_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.advanture_map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        Bundle j = intent.getExtras();
        if (j != null) {
            playerName = j.getString("playerName", "Player 1");
            TextView tvName = (TextView) findViewById(R.id.textview_scores_nam);
            tvName.setText(playerName);
        }

        initDB();

        checkpointReachedSound = MediaPlayer.create(AdvantureMapActivity.this, R.raw.checkpoint_reached);
        gameOverSound = MediaPlayer.create(AdvantureMapActivity.this, R.raw.game_over_sound);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        SharedPreferences prefs = AdvantureMapActivity.this.getSharedPreferences(
                "runsup", Context.MODE_PRIVATE);
        prefs.edit().putBoolean("endedAdventure", false).apply();

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent2) {
                Bundle bun = intent2.getExtras();
                boolean gameOver = bun.getBoolean("gameOver");
                Location loc = (Location)bun.get("location");
                double distance = bun.getDouble("distance");
                long checkpoints = bun.getLong("checkpoints");
                long duration = bun.getLong("duration");
                double score = bun.getDouble("score");
                positionList = (ArrayList<Location>) bun.get("positionList");
                Location c1 = (Location)bun.get("chaser1");
                Location c2 = (Location)bun.get("chaser2");

                if (gameOver) {
                    vibrator.vibrate(2000);
                    TextView end = findViewById(R.id.textview_end);
                    gameOverSound.start();
                    end.setVisibility(View.VISIBLE);
                    SharedPreferences prefs = AdvantureMapActivity.this.getSharedPreferences(
                            "runsup", Context.MODE_PRIVATE);
                    prefs.edit().putBoolean("endedAdventure", true).apply();
                }
                if (counter > 4) {
                    onMapReady(mMap);
                    TextView go_text = findViewById(R.id.textview_go);
                    go_text.setVisibility(View.GONE);
                    counter = 0;
                } else if (counter > -1){
                    counter++;
                    if (loc != null) {
                        if (endMarker != null) { endMarker.remove(); }
                        LatLng ltln = new LatLng(loc.getLatitude(), loc.getLongitude());
                        endMarker = mMap.addMarker(new MarkerOptions().position(ltln).title("Current location").icon(bitmapDescriptorFromVector(AdvantureMapActivity.this, R.drawable.blue_pin_person_small)));
                        // check if we are close enough to a checkpoint
                        for (int i = 0; i < checkpointLocations.size(); i++) {
                            if (checkpointLocations.get(i).distanceTo(loc) < 30) { //TODO 15
                                checkpointLocations.remove(i);
                                Marker m = checkpointMarkers.get(i);
                                checkpointMarkers.remove(m);
                                m.remove();
                                checkpoints++;
                                AdvantureService.setCheck((int)checkpoints);
                                //TODO sprobaj vibracije
                                checkpointReachedSound.start();
                                vibrator.vibrate(checkpointReachedPattern, 1);

                                if (checkpointLocations.size() < 7) { //TODO 7
                                    Toast.makeText(AdvantureMapActivity.this, "Spawning new checkpoints", Toast.LENGTH_SHORT).show();
                                    AdvantureService.setRespawnChasers();
                                    spawnCheckpointLocations(loc, 400, 9); //TODO 800, 9
                                }
                                break;
                            }
                        }
                    }
                } else {
                    if (loc != null) {
                        //INIT
                        LatLng onlyLatLng = new LatLng(loc.getLatitude(), loc.getLongitude());
                        mMap.addMarker(new MarkerOptions().position(onlyLatLng).title("Start"));
                        spawnCheckpointLocations(loc, 400, 9); //TODO 800, 9
                        counter = 5;
                    }
                }


                updateChasers(c1, c2, loc);

                TextView tvDurDist = (TextView) findViewById(R.id.textview_scores_durdist);
                TextView tvCheck = (TextView) findViewById(R.id.textview_scores_check);
                TextView tvScore = (TextView) findViewById(R.id.textview_scores_scr);

                String scoreFormat = String.valueOf(Math.round(score)) + " points";
                tvScore.setText(scoreFormat);

                String checkFormat = String.valueOf(checkpoints) + " checkpoints";
                tvCheck.setText(checkFormat);

                String durDistFormat = MainHelper.formatDuration(duration/1000) + "  " + MainHelper.formatDistance(distance) + " km";
                tvDurDist.setText(durDistFormat);
            }
        };

        Intent serviceIntent = new	Intent(this, AdvantureService.class);
        serviceIntent.setAction("si.uni_lj.fri.pbd2019.runsup.AdventureStart");
        serviceIntent.putExtra("playerName", playerName);
        startService(serviceIntent);
    }

    public void updateChasers (Location chaser1, Location chaser2, Location currentLocation) {
        if (chaser1 != null && chaser2 != null) {
            LatLng c1 = new LatLng(chaser1.getLatitude(), chaser1.getLongitude());
            LatLng c2 = new LatLng(chaser2.getLatitude(), chaser2.getLongitude());

            if (mChaserMarker1 != null) { mChaserMarker1.remove(); }
            if (!chaser1close && chaser1.distanceTo(currentLocation) < 70) {
                mChaserMarker1 = mMap.addMarker(new MarkerOptions().position(c1).title("Chaser 1").icon(bitmapDescriptorFromVector(AdvantureMapActivity.this, R.drawable.ic_android_red_30dp)));
                vibrator.vibrate(1000);
                chaser1close = true;
            } else if (!chaser1close) {
                mChaserMarker1 = mMap.addMarker(new MarkerOptions().position(c1).title("Chaser 1").icon(bitmapDescriptorFromVector(AdvantureMapActivity.this, R.drawable.ic_android_green_30dp)));
            } else if (chaser1.distanceTo(currentLocation) > 90) {
                chaser1close = false;
                mChaserMarker1 = mMap.addMarker(new MarkerOptions().position(c1).title("Chaser 1").icon(bitmapDescriptorFromVector(AdvantureMapActivity.this, R.drawable.ic_android_green_30dp)));
            } else {
                mChaserMarker1 = mMap.addMarker(new MarkerOptions().position(c1).title("Chaser 1").icon(bitmapDescriptorFromVector(AdvantureMapActivity.this, R.drawable.ic_android_red_30dp)));
            }

            if (mChaserMarker2 != null) { mChaserMarker2.remove(); }
            if (!chaser2close && chaser2.distanceTo(currentLocation) < 70) {
                mChaserMarker2 = mMap.addMarker(new MarkerOptions().position(c2).title("Chaser 2").icon(bitmapDescriptorFromVector(AdvantureMapActivity.this, R.drawable.ic_android_red_30dp)));
                vibrator.vibrate(1000);
                chaser2close = true;
            } else if (!chaser2close) {
                mChaserMarker2 = mMap.addMarker(new MarkerOptions().position(c2).title("Chaser 2").icon(bitmapDescriptorFromVector(AdvantureMapActivity.this, R.drawable.ic_android_green_30dp)));
            } else if (chaser2.distanceTo(currentLocation) > 90) {
                chaser2close = false;
                mChaserMarker2 = mMap.addMarker(new MarkerOptions().position(c2).title("Chaser 2").icon(bitmapDescriptorFromVector(AdvantureMapActivity.this, R.drawable.ic_android_green_30dp)));
            } else {
                mChaserMarker2 = mMap.addMarker(new MarkerOptions().position(c2).title("Chaser 2").icon(bitmapDescriptorFromVector(AdvantureMapActivity.this, R.drawable.ic_android_red_30dp)));
            }
        } else {
            Log.d("sweapper", "chaserja sta = NULL");
        }

    }

    public void spawnCheckpointLocations(Location myLocation, int radius, int n) {
        // remove markers from before if they exist
        if (checkpointMarkers != null) { for (Marker m : checkpointMarkers) { m.remove(); } }

        checkpointMarkers = new ArrayList<>();
        checkpointLocations = new ArrayList<>();
        double x0 = myLocation.getLatitude();
        double y0 = myLocation.getLongitude();
        double radiusInDegrees = radius / 111000f;

        while (checkpointLocations.size() < n) {
            Random random = new Random();
            // convert radius from meters to degrees
            double u = random.nextDouble();
            double v = random.nextDouble();
            double w = radiusInDegrees * Math.sqrt(u);
            double t = 2 * Math.PI * v;
            double x = w * Math.cos(t);
            double y = w * Math.sin(t);

            // adjust the x-coordinate for the shrinking of the east-west distances
            double new_x = x / Math.cos(y0);

            double foundLatitude = new_x + x0;
            double foundLongitude = y + y0;
            LatLng randomLatLng = new LatLng(foundLatitude, foundLongitude);
            Location randomLocation = new Location("");
            randomLocation.setLatitude(randomLatLng.latitude);
            randomLocation.setLongitude(randomLatLng.longitude);

            // check if the new location is ok (far enough from the original location and all the other locations
            if (myLocation.distanceTo(randomLocation) < radius/5 || randomLocation.distanceTo(myLocation) > radius) {
                continue;
            } else {
                boolean good = true;
                for (Location c : checkpointLocations) {
                    if (c.distanceTo(randomLocation) < radius/5) {
                        good = false;
                    }
                }
                if (!good) { continue; }
            }

            // good checkpoint locations
            Marker m = mMap.addMarker(new MarkerOptions().position(randomLatLng).title("Checkpoint").icon(bitmapDescriptorFromVector(AdvantureMapActivity.this, R.drawable.pin_advanture_small)));
            checkpointMarkers.add(m);
            checkpointLocations.add(randomLocation);
        }
        for (Location c : checkpointLocations) {
            Log.d("sweapper", String.valueOf(c.distanceTo(myLocation)));
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void initDB () {
        dbHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try {
            advantureDao = dbHelper.advantureDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder .setTitle(R.string.start_advanture)
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SharedPreferences prefs = AdvantureMapActivity.this.getSharedPreferences(
                                "runsup", Context.MODE_PRIVATE);
                        prefs.edit().putBoolean("endedAdventure", true).apply();
                        AdvantureMapActivity.this.finish();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void onBack(View view) {
        Log.d("sweapper", "BACK");
        onBackPressed();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng ltln = narisiGpsPot();
        if (ltln != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ltln, 16)); //TODO 15
        }
    }

    public LatLng narisiGpsPot () {
        PolylineOptions options = new PolylineOptions().width(6).color(Color.CYAN).geodesic(true);
        LatLng prevLatLng = null;
        if (positionList.size() > 0) {
            Location firstLocation = positionList.get(0);
            LatLng firstLatLng = new LatLng(firstLocation.getLatitude(), firstLocation.getLongitude());
            mMap.addMarker(new MarkerOptions().position(firstLatLng).title("Start"));
            for (Location loc : positionList) {
                LatLng curLatLng = new LatLng(loc.getLatitude(),loc.getLongitude());
                options.add(curLatLng);
                prevLatLng = curLatLng;
            }
            if (endMarker != null) { endMarker.remove(); }
            endMarker = mMap.addMarker(new MarkerOptions().position(prevLatLng).title("Current location").icon(bitmapDescriptorFromVector(AdvantureMapActivity.this, R.drawable.blue_pin_person_small)));
            polyLine = mMap.addPolyline(options);
            return prevLatLng;
        }
        return null;
    }


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mBroadcastReceiver, new IntentFilter("si.uni_lj.fri.pbd2019.runsup.AdventureUpdate"));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mBroadcastReceiver);
    }
}
