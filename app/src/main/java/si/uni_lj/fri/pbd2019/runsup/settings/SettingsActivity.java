package si.uni_lj.fri.pbd2019.runsup.settings;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;

import si.uni_lj.fri.pbd2019.runsup.R;

public class SettingsActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    static final int REQUEST_ID_LOCATION_PERMISSIONS = 0;
    public static Boolean allowLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pref_main);
        setTitle("Settings");

        Switch s = (Switch) findViewById(R.id.location_switch);

        final SharedPreferences prefs = getSharedPreferences(
                "runsup", Context.MODE_PRIVATE);
        if (!prefs.getBoolean("locationEnabled", false)) {
            s.setChecked(false);
        } else {
            s.setChecked(true);
        }
        s.setOnCheckedChangeListener(this);

        final Switch s_unit = (Switch) findViewById(R.id.unit_switch);
        if (prefs.getInt("unit", 0) == 0) {
            s_unit.setChecked(false);
        } else {
            s_unit.setChecked(true);
        }

        s_unit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!s_unit.isChecked()) {
                    //km
                    //TODO posodobi vse unite
                    prefs.edit().putInt("unit", 0).apply();
                } else {
                    //mi
                    prefs.edit().putInt("unit", 1).apply();
                }
            }
        });
    }

    public void reqLocation() {
        ActivityCompat.requestPermissions(SettingsActivity.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_ID_LOCATION_PERMISSIONS);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked) {
            reqLocation();
            SharedPreferences prefs = getSharedPreferences(
                    "runsup", Context.MODE_PRIVATE);
            prefs.edit().putBoolean("locationEnabled", true).apply();
        } else {
            SharedPreferences prefs = getSharedPreferences(
                    "runsup", Context.MODE_PRIVATE);
            prefs.edit().putBoolean("locationEnabled", false).apply();
        }
    }
}
