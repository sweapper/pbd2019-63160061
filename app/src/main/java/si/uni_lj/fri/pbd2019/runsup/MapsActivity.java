package si.uni_lj.fri.pbd2019.runsup;

import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    //DATABASE
    DatabaseHelper dbHelper;
    Dao<GpsPoint,Long> gpsPointDao;

    private GoogleMap mMap;
    Polyline line;
    private long workoutId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_maps_map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        workoutId = intent.getLongExtra("workoutId", -1);
        initDB();
    }

    private void initDB () {
        dbHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try {
            gpsPointDao = dbHelper.gpsPointDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        List<GpsPoint> gpsPoints = new ArrayList<>();

        QueryBuilder queryBuilder = gpsPointDao.queryBuilder();
        try {
            queryBuilder.where()
                    .eq("workout_id", workoutId);
            gpsPoints = gpsPointDao.query(queryBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        LatLngBounds.Builder builder = narisiGpsPot(gpsPoints);
        LatLngBounds bounds = builder.build();
        int padding = 20;
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.moveCamera(cu);
    }

    public LatLngBounds.Builder narisiGpsPot (List<GpsPoint> positionList) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        PolylineOptions options = new PolylineOptions().width(6).color(Color.CYAN).geodesic(true);
        LatLng prevLatLng = null;
        GpsPoint prevGpsPoint = null;
        for (GpsPoint gpsPoint : positionList) {
            LatLng curLatLng = new LatLng(gpsPoint.getLatitude(),gpsPoint.getLongitude());

            if (prevLatLng == null) {
                mMap.addMarker(new MarkerOptions().position(curLatLng).title("Start"));
                options.add(curLatLng);
                builder.include(curLatLng);
            } else if (!prevLatLng.equals(curLatLng)) {
                if (gpsPoint.getSessionNumber() != prevGpsPoint.getSessionNumber()){
                    Location loc1 = new Location("");
                    loc1.setLatitude(gpsPoint.getLatitude());
                    loc1.setLongitude(gpsPoint.getLongitude());
                    Location loc2 = new Location("");
                    loc2.setLatitude(prevGpsPoint.getLatitude());
                    loc2.setLongitude(prevGpsPoint.getLongitude());
                    if (loc1.distanceTo(loc2) > 100) {
                        mMap.addMarker(new MarkerOptions().position(prevLatLng)
                                .title("Pause " + String.valueOf(prevGpsPoint.getSessionNumber() + 1)));
                        mMap.addMarker(new MarkerOptions().position(curLatLng)
                                .title("Continue " + String.valueOf(gpsPoint.getSessionNumber() + 1)));
                        // prekinemo polyline
                        line = mMap.addPolyline(options);
                        options = new PolylineOptions().width(6).color(Color.CYAN).geodesic(true);
                    } else {
                        mMap.addMarker(new MarkerOptions().position(prevLatLng)
                                .title("Break " + String.valueOf(prevGpsPoint.getSessionNumber() + 1)));
                    }
                }
                options.add(curLatLng);
                builder.include(curLatLng);
            }
            prevLatLng = curLatLng;
            prevGpsPoint = gpsPoint;
        }
        mMap.addMarker(new MarkerOptions().position(prevLatLng).title("End"));
        line = mMap.addPolyline(options);
        return builder;
    }
}
