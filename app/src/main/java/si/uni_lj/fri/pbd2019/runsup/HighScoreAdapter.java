package si.uni_lj.fri.pbd2019.runsup;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.Advanture;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;

public class HighScoreAdapter extends ArrayAdapter<Advanture> {

    private Context mContext;
    private int mResource;

    public HighScoreAdapter(@NonNull Context context, int resource, List<Advanture> allAdvantures) {
        super(context, resource, allAdvantures);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String name = getItem(position).getName();
        Long duration = getItem(position).getDuration();
        Double distance = getItem(position).getDistance();
        Long checkpoints = getItem(position).getCheckpoints();
        Double score = getItem(position).getScore();
        long place = getItem(position).getPosition();


        DateFormat konecWorkouta = SimpleDateFormat.getDateTimeInstance();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView tvName = (TextView) convertView.findViewById(R.id.textview_scores_name);
        TextView tvDurDist = (TextView) convertView.findViewById(R.id.textview_scores_duration_distance);
        TextView tvCheck = (TextView) convertView.findViewById(R.id.textview_scores_checkpoints);
        TextView tvScore = (TextView) convertView.findViewById(R.id.textview_scores_score);
        TextView tvPlace = (TextView) convertView.findViewById(R.id.textview_scores_place);


        String placeFormat = String.valueOf(place) + ".";
        tvPlace.setText(placeFormat);

        tvName.setText(name);

        String scoreFormat = String.valueOf(Math.round(score)) + " points";
        tvScore.setText(scoreFormat);

        String checkFormat = String.valueOf(checkpoints) + " checkpoints";
        tvCheck.setText(checkFormat);

        String durDistFormat = MainHelper.formatDuration(duration/1000) + "  " + MainHelper.formatDistance(distance) + " km";
        tvDurDist.setText(durDistFormat);

        return convertView;
    }
    //setContentView(R.layout.adapter_history);
}
