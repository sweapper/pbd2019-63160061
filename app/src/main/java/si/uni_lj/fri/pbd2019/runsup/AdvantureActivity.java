package si.uni_lj.fri.pbd2019.runsup;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.stetho.Stetho;
import com.facebook.stetho.inspector.protocol.module.Database;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.Advanture;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class AdvantureActivity extends AppCompatActivity {

    //DATABASE
    DatabaseHelper dbHelper;
    Dao<Advanture,Long> advantureDao;

    static final int REQUEST_ID_LOCATION_PERMISSIONS = 0;

    private String playerName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advanture);
        setTitle("Adventure time!");
        //android.os.Process.killProcess(android.os.Process.myPid());
        initDB();
        dodajNoveIgralce();
        dodajOnClick();
        reqLocation();
    }

    public void reqLocation() {
        ActivityCompat.requestPermissions(AdvantureActivity.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_ID_LOCATION_PERMISSIONS);
    }

    private void initDB () {
        dbHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
//        SQLiteDatabase db = dbHelper.getWritableDatabase();
//        dbHelper.onCreate(db);
        try {
            advantureDao = dbHelper.advantureDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void dodajNoveIgralce() {
        List<Advanture> vsiIgralci = new ArrayList<>();
        try {
            vsiIgralci = advantureDao.queryForAll();
            if (vsiIgralci.size() == 0) { ustvariRobote(); }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void ustvariRobote () {
        Advanture novVnos1 = new Advanture(
                "Slow Robot",
                15000,
                50.0,
                0
        );
        Advanture novVnos2 = new Advanture(
                "Cool Robot",
                180000,
                500.0,
                1
        );
        Advanture novVnos3 = new Advanture(
                "Fast Robot",
                1200000,
                1500.0,
                3
        );
        try {
            advantureDao.create(novVnos1);
            advantureDao.create(novVnos2);
            advantureDao.create(novVnos3);
        } catch (SQLException e) {
            Log.d("kurko", "novGpsPint NI kreiran!");
            Toast.makeText(AdvantureActivity.this, "SQLite database error! - no table named Advanture!", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void dodajOnClick() {
        final Button start = (Button) findViewById(R.id.start_advanture);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText name = (EditText) findViewById(R.id.player_name);
                playerName = name.getText().toString();
                if (playerName.equals("")) {
                    new AlertDialog.Builder(AdvantureActivity.this)
                            .setMessage(R.string.please_enter_name)
                            .setPositiveButton(R.string.ok, null)
                            .show();
                } else {
                    reqLocation();
                    new AlertDialog.Builder(AdvantureActivity.this)
                            .setTitle(R.string.start_advanture)
                            .setMessage(playerName + " you are about to start a new adventure! Proceed?")
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int a) {
                                    if (ActivityCompat.checkSelfPermission(AdvantureActivity.this,Manifest.permission.ACCESS_FINE_LOCATION) ==
                                            PackageManager.PERMISSION_GRANTED &&
                                            ActivityCompat.checkSelfPermission(AdvantureActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                                                    PackageManager.PERMISSION_GRANTED) {
                                        Intent intent = new Intent(AdvantureActivity.this, AdvantureMapActivity.class);
                                        intent.putExtra("playerName", playerName);
                                        startActivity(intent);
                                    } else {
                                        Toast.makeText(AdvantureActivity.this, "To start an adventure you have to enable location.", Toast.LENGTH_LONG).show();
                                        // reqLocation();
                                    }
                                }
                            })
                            .setNegativeButton(R.string.no, null)
                            .show();
                }
            }
        });

        final Button instructions = (Button) findViewById(R.id.about_advanture);
        instructions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdvantureActivity.this, InstructionsActivity.class);
                startActivity(intent);
            }
        });

        final Button scores = (Button) findViewById(R.id.history_advanture);
        scores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdvantureActivity.this, HighScoresActivity.class);
                startActivity(intent);
            }
        });
    }
}
