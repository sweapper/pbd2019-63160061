package si.uni_lj.fri.pbd2019.runsup.services;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import si.uni_lj.fri.pbd2019.runsup.AdvantureMapActivity;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.model.Advanture;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class AdvantureService extends Service {


    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();
    private FusedLocationProviderClient mFLPC;
    private Handler mHandler;
    private final int DELAY = 1000;
    private boolean gameOver = false;

    private double distance = 0.0;
    private static long checkpoints = 0;
    private static boolean respawnChasers = false;
    private long duration = 0;
    private long adventureStart;
    private double score;
    private String playerName;
    Advanture avantura;



    LocationRequest mLocationRequest;
    LocationCallback mLocationCallback;
    boolean mRequestingLocationUpdates = false;
    Location mCurrentLocation;
    private Location mChaserLocation1;
    private Location mChaserLocation2;
    private ArrayList<Location> positionList = new ArrayList<>();

    //DATABASE
    DatabaseHelper dbHelper;
    Dao<Advanture,Long> advantureDao;


    final Runnable r = new Runnable() {
        public void	run() {
            SharedPreferences prefs = AdvantureService.this.getSharedPreferences(
                    "runsup", Context.MODE_PRIVATE);
            Boolean ended = prefs.getBoolean("endedAdventure", false);
            if (!ended) posodobiWorkout();
            else {
                positionList = new ArrayList<>();
                onDestroy();
            }

            mHandler.postDelayed(this, DELAY);
        }
    };

    public static void setCheck(int n) {
        checkpoints = n;
    }

    public static void setRespawnChasers() {
        respawnChasers = true;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mFLPC = LocationServices.getFusedLocationProviderClient(this);
        mHandler = new Handler();

        initDB();
    }

    private void initDB () {
        dbHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try {
            advantureDao = dbHelper.advantureDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public AdvantureService getService() {
            // Return this instance of LocalService so clients can call public methods
            return AdvantureService.this;
        }
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setSmallestDisplacement(10);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                mCurrentLocation = locationResult.getLastLocation();
                if (positionList.size() > 1) {
                    Location lastLocation = positionList.get(positionList.size()-1);
                    distance += lastLocation.distanceTo(mCurrentLocation);
                } else if (positionList.size() == 0) {
                    initChasers(mCurrentLocation);
                }
                if (respawnChasers) { initChasers(mCurrentLocation); }
                positionList.add(mCurrentLocation);
            }
        };
    }

    private void initChasers(Location myLocation) {
        int radius = 500;

        double x0 = myLocation.getLatitude();
        double y0 = myLocation.getLongitude();
        double radiusInDegrees = radius / 111000f;

        int initedChasers = 0;

        while (initedChasers < 2) {
            Random random = new Random();
            // convert radius from meters to degrees
            double u = random.nextDouble();
            double v = random.nextDouble();
            double w = radiusInDegrees * Math.sqrt(u);
            double t = 2 * Math.PI * v;
            double x = w * Math.cos(t);
            double y = w * Math.sin(t);

            // adjust the x-coordinate for the shrinking of the east-west distances
            double new_x = x / Math.cos(y0);

            double foundLatitude = new_x + x0;
            double foundLongitude = y + y0;
            LatLng randomLatLng = new LatLng(foundLatitude, foundLongitude);
            Location randomLocation = new Location("");
            randomLocation.setLatitude(randomLatLng.latitude);
            randomLocation.setLongitude(randomLatLng.longitude);

            // check if the new location is ok (far enough from the original location and all the other locations
            if (myLocation.distanceTo(randomLocation) < radius/2 || myLocation.distanceTo(randomLocation) > radius) {
                continue;
            } else if (initedChasers == 1) {
                if (mChaserLocation1.distanceTo(randomLocation) < radius/2) {
                    continue;
                } else {
                    mChaserLocation2 = randomLocation;
                    return;
                }
            }
            mChaserLocation1 = randomLocation;
            initedChasers++;
        }
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            Log.e("PEPE", "Location access denied!");
            return;
        }
        createLocationRequest();
        createLocationCallback();
        mRequestingLocationUpdates = true;
        mFLPC.requestLocationUpdates(mLocationRequest,mLocationCallback, Looper.myLooper());
    }

    private void stopLocationUpdates() {
        if (!mRequestingLocationUpdates) return;
        mRequestingLocationUpdates = false;
        mFLPC.removeLocationUpdates(mLocationCallback);
    }

    public void zacniWorkout() {
        mHandler.postDelayed(r, DELAY);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacks(r);
        stopLocationUpdates();
        stopSelf();
        Log.d("sweapper", "mrtu service");
    }

    public void posodobiWorkout() {
        this.duration = (SystemClock.uptimeMillis() - adventureStart);
        Intent intent = new Intent();
        intent.setAction("si.uni_lj.fri.pbd2019.runsup.AdventureUpdate");
        intent.putExtra("duration", this.duration);
        intent.putExtra("gameOver", this.gameOver);

        if (mCurrentLocation != null) {
            intent.putExtra("location", this.mCurrentLocation);
            posodobiChaserje(mCurrentLocation);
        } else {
            Log.d("sweapper", "Location is NULL");
            Toast.makeText(AdvantureService.this, "Cannot get location!", Toast.LENGTH_SHORT).show();
        }
        intent.putExtra("distance", this.distance);
        intent.putExtra("checkpoints", this.checkpoints);

        List<Advanture> lastAdventure = new ArrayList<>();
        try {
            lastAdventure = advantureDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (lastAdventure.size() == 0) {
            avantura = lastAdventure.get(lastAdventure.size());
        }

        avantura.setCheckpoints(checkpoints);
        avantura.setDistance(distance);
        avantura.setDuration(duration);
        avantura.setScore(duration, distance, checkpoints);
        this.score = avantura.getScore();

        intent.putExtra("positionList", this.positionList);
        intent.putExtra("score", this.score);
        intent.putExtra("chaser1", this.mChaserLocation1);
        intent.putExtra("chaser2", this.mChaserLocation2);

        try {
            advantureDao.update(avantura);
        } catch (SQLException e) {
            Log.e("sweapper", "Adventure se ni posodobil!");
            e.printStackTrace();
        }
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private void posodobiChaserje(Location myLocation) {
        double radius = 2.3;
        int randRadius = 1;

        double x0 = myLocation.getLatitude();
        double y0 = myLocation.getLongitude();
        double x1 = mChaserLocation1.getLatitude();
        double y1 = mChaserLocation1.getLongitude();
        double x2 = mChaserLocation2.getLatitude();
        double y2 = mChaserLocation2.getLongitude();

        int lat1;
        if (x0 - x1 > 0) { lat1 = -1; } else { lat1 = 1; }
        int lon1;
        if (y0 - y1 > 0) { lon1 = -1; } else { lon1 = 1; }

        int lat2;
        if (x0 - x2 > 0) { lat2 = -1; } else { lat2 = 1; }
        int lon2;
        if (y0 - y2 > 0) { lon2 = -1; } else { lon2 = 1; }

        double radiusInDegrees = radius / 111000f;
        double randRadiusInDegrees = randRadius / 111000f;

        Random random = new Random();
        // convert radius from meters to degrees
        double u = random.nextDouble();
        double v = random.nextDouble();
        double w = randRadiusInDegrees * Math.sqrt(u);
        double t = 2 * Math.PI * v;
        double xr1 = w * Math.cos(t);
        double yr1 = w * Math.sin(t);

        double u2 = random.nextDouble();
        double v2 = random.nextDouble();
        double w2 = randRadiusInDegrees * Math.sqrt(u2);
        double t2 = 2 * Math.PI * v2;
        double xr2 = w2 * Math.cos(t2);
        double yr2 = w2 * Math.sin(t2);

        double x = radiusInDegrees / Math.cos(y0);
        double y = - radiusInDegrees;

        double newLat1 = x*lat1 + x1 + xr1;
        double newLon1 = y*lon1 + y1 + yr1;

        double newLat2 = x*lat2 + x2 + xr2;
        double newLon2 = y*lon2 + y2 + yr2;

        mChaserLocation1.setLatitude(newLat1);
        mChaserLocation1.setLongitude(newLon1);

        mChaserLocation2.setLatitude(newLat2);
        mChaserLocation2.setLongitude(newLon2);

        if (mChaserLocation1.distanceTo(myLocation) < 10 ||
                mChaserLocation2.distanceTo(myLocation) < 10) { gameOver = true; }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String akcija = intent.getAction();
        if (akcija != null) {
            switch (akcija) {
                case "si.uni_lj.fri.pbd2019.runsup.AdventureStart":
                    Bundle j = intent.getExtras();
                    if (j != null) {
                        playerName = j.getString("playerName", "Player 1");
                    }
                    avantura = new Advanture(playerName);
                    try {
                        advantureDao.create(avantura);
                    } catch (SQLException e) {
                        Log.e("sweapper", "Adventure ni kreiran!");
                        e.printStackTrace();
                    }
                    gameOver = false;
                    this.duration = 0;
                    this.adventureStart = SystemClock.uptimeMillis();
                    startLocationUpdates();
                    zacniWorkout();
                    break;

                case "si.uni_lj.fri.pdb2019.runsup.AdventureEnd":
                    Log.e("sweapper", "UMIRAM");
                    stopLocationUpdates();
                    stopSelf();
                    break;
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }
}