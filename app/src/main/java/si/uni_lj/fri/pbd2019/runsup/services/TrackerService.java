package si.uni_lj.fri.pbd2019.runsup.services;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Binder;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.MapsActivity;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.StopwatchActivity;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.UserProfile;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

import static com.google.android.gms.plus.PlusOneDummyView.TAG;

public class TrackerService extends Service {


    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();
    private FusedLocationProviderClient mFLPC;
    private Handler mHandler;
    private final int DELAY = 1000;

    private int sportActivity = 0;
    private double distance = 0.0;
    private double calories = 0.0;
    private long duration = 0;
    private long workoutStart;
    private long workoutId;
    private String userId;
    private static int state;
    private long prevDruation;
    private double pace;
    private int sessionNumber = 0;

    static final int STATE_STOPPED = 0;
    static final int STATE_RUNNING = 1;
    static final int STATE_PAUSED = 2;
    static final int STATE_CONTINUE = 3;

    int inactivityCounter = 0;
    LocationRequest mLocationRequest;
    LocationCallback mLocationCallback;
    boolean mRequestingLocationUpdates = false;
    Location mCurrentLocation;
    Location mPrevLocation;
    private ArrayList<Location> positionList = new ArrayList<Location>();
    private List<Float> speedList = new ArrayList<Float> ();

    //DATABASE
    DatabaseHelper dbHelper;
    Dao<GpsPoint,Long> gpsPointDao;
    Dao<Workout,Long> workoutDao;
    Dao<User,Long> userDao;
    Dao<UserProfile,Long> userProfileDao;

    // duration
    private float speed;
    // pace
    // calories
    private Date created;
    private Date lastUpdate;

    private User user;
    private UserProfile userProfile;
    private Workout workout;
    private String title;
    // state = status

    public int getState() { return this.state; }
    public long getDuration() { return this.duration; }
    public double getDistance() { return this.distance; }
    public double getPace() { return this.pace; }

    @Override
    public void onCreate() {
        super.onCreate();
        mFLPC = LocationServices.getFusedLocationProviderClient(this);
        mHandler = new Handler();

        initDB();

        /*Test
        try {
            List<GpsPoint> test = gpsPointDao.queryForEq("id", 33);
            Log.d("brkoxx", test.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        */
    }

    private void initDB () {
        dbHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try {
            gpsPointDao = dbHelper.gpsPointDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            workoutDao = dbHelper.workoutDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            userDao = dbHelper.userDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            userProfileDao = dbHelper.userProfileDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public TrackerService getService() {
            // Return this instance of LocalService so clients can call public methods
            return TrackerService.this;
        }
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(3000);
        mLocationRequest.setSmallestDisplacement(10);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                mCurrentLocation = locationResult.getLastLocation();
                if (positionList.size() > 1) {
                    Location lastLocation = positionList.get(positionList.size()-1);
                    distance += lastLocation.distanceTo(mCurrentLocation);
                }
                //Log.d(TAG, mCurrentLocation.toString());
                positionList.add(mCurrentLocation);
                //Log.e("calories", Float.toString(mCurrentLocation.getSpeed()));

                //novGpsPoint(mCurrentLocation);
                speed = mCurrentLocation.getSpeed();
                speedList.add(speed);
                //TODO to baje treba samo na location change posiljat
            }
        };
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e("PEPE", "Location access denied!");
            return;
        }
        createLocationRequest();
        createLocationCallback();
        mRequestingLocationUpdates = true;
        mFLPC.requestLocationUpdates(mLocationRequest,mLocationCallback, Looper.myLooper());
    }

    private void stopLocationUpdates() {
        if (!mRequestingLocationUpdates) return;
        mRequestingLocationUpdates = false;
        mFLPC.removeLocationUpdates(mLocationCallback);
    }

    public void zacniWorkout() {
        mHandler.postDelayed(new Runnable() {
            public void	run() {
                state = getState();
                if (state == STATE_RUNNING || state == STATE_CONTINUE)
                    posodobiWorkout();
                mHandler.postDelayed(this, DELAY);
            }
        }, DELAY);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("kurko", "mrtu service");
    }

    public void posodobiWorkout() {
        this.duration = (SystemClock.uptimeMillis() - workoutStart + prevDruation);
        Intent intent = new Intent();
        intent.setAction("si.uni_lj.fri.pbd2019.runsup.TICK");
        intent.putExtra("duration", this.duration);

        Intent activeIntent = new Intent();
        activeIntent.setAction("si.uni_lj.fri.pbd2019.runsup.ACTIVE_TICK");
        activeIntent.putExtra("state", this.state);

        if ((mPrevLocation == null || mPrevLocation != mCurrentLocation) && mCurrentLocation != null) {
            intent.putExtra("location", this.mCurrentLocation);
            // ce se posodobi lokacija je treba updatat activeWorkout
            activeIntent.putExtra("location", this.mCurrentLocation);
            mPrevLocation = mCurrentLocation;
            novGpsPoint(mPrevLocation);
            inactivityCounter = 0;
            //TODO posodobi workout
            updateWorkoutDB();
        } else {
            intent.putExtra("location", (String)null);
            inactivityCounter++;
            if (inactivityCounter >= 10) {
                inactivityCounter = 0;
                updateWorkoutDB();
            }
        }
        intent.putExtra("workoutId", this.workoutId);
        intent.putExtra("distance", this.distance);
        intent.putExtra("state", this.state);

        if (inactivityCounter < 6) {
            pace = distance / (duration / 60000.0);
        } else {
            pace = 0;
        }
        intent.putExtra("pace", this.pace);

        this.calories = SportActivities.countCalories(sportActivity, -1, speedList, duration/3600000.0);
        intent.putExtra("calories", this.calories);
        intent.putExtra("speed", this.speed);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(activeIntent);
    }

    public void posodobiStanje() {
        Intent intent = new Intent();
        intent.setAction("si.uni_lj.fri.pbd2019.runsup.TICK");
        intent.putExtra("state", state);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    public void novGpsPoint (Location mLokacija) {
        GpsPoint novVnos = new GpsPoint(
                workout,
                sessionNumber,
                mLokacija,
                duration,
                speed,
                pace,
                calories
        );
        try {
            gpsPointDao.create(novVnos);
        } catch (SQLException e) {
            Log.d("kurko", "novGpsPint NI kreiran!");
            e.printStackTrace();
        }
    }

    public void updateWorkoutDB() {
        List<Workout> lastWorkout = new ArrayList<>();
        try {
            lastWorkout = workoutDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        workout = lastWorkout.get(lastWorkout.size()-1);
        updateStateDB();
        if (workout != null) {
            Log.d("kurko", "MOJID " + String.valueOf(workout.getId()));
            workout.setDistance(distance);
            workout.setStatus(state);
            workout.setDuration(duration);
            workout.setTotalCalories(calories);
            workout.setPaceAvg(pace);
            workout.setLastUpdate(new Date());
            try {
                workoutDao.update(workout);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            Log.d("kurko", "NULL JE");
        }
    }

    public void updateStateDB() {
        if (workout != null) {
            workout.setStatus(state);
            workout.setLastUpdate(new Date());
            try {
                workoutDao.update(workout);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            Log.d("kurko", "NULL JE");
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String akcija = intent.getAction();
        List<Workout> lastWorkout = new ArrayList<>();
        if (akcija != null) {
            switch (akcija) {
                case "si.uni_lj.fri.pbd2019.runsup.COMMAND_START":
                    state = STATE_RUNNING;
                    SharedPreferences prefs = getSharedPreferences(
                            "runsup", Context.MODE_PRIVATE);
                    prefs.edit().putBoolean("startedWorkout", true).apply();
                    sessionNumber = 0;
                    int workoutNumber = 1;
                    try {
                        List<Workout> allWorkouts = workoutDao.queryForAll();
                        if (allWorkouts.size() > 0) {
                            workoutNumber = allWorkouts.size() + 1;
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    workout = new Workout();
                    workout.setTitle("Workout " + String.valueOf(workoutNumber));
                    workout.setCreated(new Date());
                    workout.setStatus(STATE_RUNNING);
                    workout.setSportActivity(sportActivity);
                    workout.setLastUpdate(new Date());
                    workout.setEnded(false);

                    Bundle j = intent.getExtras();
                    userId = j.getString("accId", "");
                    if (!userId.equals("")) {
                        try {
                            List<User> users = userDao.queryForEq("accId", userId);
                            if (!users.isEmpty()) {
                                user = users.get(0);
                                workout.setUser(user);
                            }
                        } catch (SQLException e) {
                            Log.d("kurko", "Ne najdem userja!");
                            e.printStackTrace();
                        }
                    }
                    try {
                        workoutDao.create(workout);
                    } catch (SQLException e) {
                        Log.d("kurko", "Workout NI kreiran!");
                        e.printStackTrace();
                    }
                    this.workoutId = workout.getId();
                    this.duration = 0;
                    this.workoutStart = SystemClock.uptimeMillis();
                    startLocationUpdates();
                    zacniWorkout();
                    break;
                case "si.uni_lj.fri.pbd2019.runsup.COMMAND_CONTINUE":
                    state = STATE_CONTINUE;
                    try {
                        lastWorkout = workoutDao.queryForAll();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    workout = lastWorkout.get(lastWorkout.size()-1);
                    updateStateDB();
                    sessionNumber++;
                    startLocationUpdates();
                    this.workoutStart = SystemClock.uptimeMillis();
                    break;
                case "si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE":
                    state = STATE_PAUSED;
                    try {
                        lastWorkout = workoutDao.queryForAll();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    workout = lastWorkout.get(lastWorkout.size()-1);
                    updateStateDB();
                    prevDruation += (SystemClock.uptimeMillis() - this.workoutStart);
                    posodobiStanje();
                    stopLocationUpdates();
                    break;
                case "si.uni_lj.fri.pbd2019.runsup.COMMAND_STOP":
                    state = STATE_STOPPED;
                    try {
                        lastWorkout = workoutDao.queryForAll();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    workout = lastWorkout.get(lastWorkout.size()-1);
                    //Log.d() adobi sploh taprav workout? a bi res to posodablu sploh...
                    Log.d("kurko", String.valueOf(duration));
                    updateStateDB();
                    prevDruation += (SystemClock.uptimeMillis() - this.workoutStart);
                    //posodobiStanje();
                    stopLocationUpdates();
                    break;
                case "si.uni_lj.fri.pdb2019.runsup.UPDATE_SPORT_ACTIVITY":
                    this.sportActivity = intent.getIntExtra("sportActivity", -1);
                    break;
                case "si.uni_lj.fri.pdb2019.runsup.KILL":
                    Log.e("kurko", "UMIRAM");
                    stopSelf();
                    break;
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }
}