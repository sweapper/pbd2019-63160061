package si.uni_lj.fri.pbd2019.runsup.helpers;

import android.util.Log;

import java.util.List;

public final class SportActivities {
    public static final int RUNNING = 0;
    public static final int WALKING = 1;
    public static final int CYCLING = 2;

    //TODO a je treba pretvort m/s v mph? al je to napaka?
    public static double getMET(int activityType, Float speed) {
        double sspeed = MainHelper.mpsToMiph(speed);
        double hitrost = Math.ceil(sspeed);
        switch (activityType) {
            // running
            case 0:
                if (hitrost == 4) return 6.0;
                else if (hitrost == 5) return 8.3;
                else if (hitrost == 6) return 9.8;
                else if (hitrost == 7) return 11.0;
                else if (hitrost == 8) return 11.8;
                else if (hitrost == 9) return 12.8;
                else if (hitrost == 10) return 14.5;
                else if (hitrost == 11) return 16.0;
                else if (hitrost == 12) return 19.0;
                else if (hitrost == 13) return 19.8;
                else if (hitrost == 14) return 23.0;
                else return sspeed * 1.535353535;
            // walking intent.createChooser; intent action je drug; getIntent getExtra get
            case 1:
                if (hitrost == 1) return 2.0;
                else if (hitrost == 2) return 2.8;
                else if (hitrost == 3) return 3.1;
                else if (hitrost == 4) return 3.5;
                else return sspeed * 1.14;
            // cycling
            // TODO kaj je fora če je vmes - npr. na 11?
            case 2:
                if (hitrost == 10) return 6.8;
                else if (hitrost == 12) return 8.0;
                else if (hitrost == 14) return 10.0;
                else if (hitrost == 16) return 12.8;
                else if (hitrost == 18) return 13.6;
                else if (hitrost == 20) return 15.8;
                else return sspeed * 0.744444444;
        }
        return 0.0;
    }

    public static double countCalories(int sportActivity, float weight, List<Float> speedList, double timeFillingSpeedListInHours) {
        // default = 60
        if (weight == -1) {
            weight = 60;
        }
        float sum = 0;
        int n = 1;
        for (float i : speedList)
            sum = sum + i;

        n = speedList.size();
        double met = getMET(sportActivity, sum/n);
        return weight * timeFillingSpeedListInHours * met;
    }

    public static String getSportActivityName (int activity) {
        switch (activity) {
            case 0:
                return "running";
            case 1:
                return  "walking";
            case 2:
                return "cycling";
        }
        return "ERROR";
    }
}
