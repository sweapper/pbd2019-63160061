package si.uni_lj.fri.pbd2019.runsup.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Advanture {

    public Advanture () {}

    public Advanture (String name) {
        this.name = name;
    }

    public Advanture(String name, long duration, double distance, long checkpoints){
        this.name = name;
        this.duration = duration;
        this.distance = distance;
        this.checkpoints = checkpoints;
        this.score = duration * 0.0005 + distance * 0.2 + checkpoints * 500;
    }

    @DatabaseField(generatedId = true)
    private long id;

    @DatabaseField
    private String name;

    @DatabaseField
    private long duration;

    @DatabaseField
    private double distance;

    @DatabaseField
    private long checkpoints;

    @DatabaseField
    private double score;

    @DatabaseField
    private int position;


    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public long getCheckpoints() {
        return checkpoints;
    }

    public void setCheckpoints(long checkpoints) {
        this.checkpoints = checkpoints;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public void setScore(long duration, double distance, long checkpoints) {
        this.score = duration * 0.0005 + distance * 0.2 + checkpoints * 500;
    }

    public long getPosition() { return position; }

    public void setPosition(int position) { this.position = position; }
}
