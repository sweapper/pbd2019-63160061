package si.uni_lj.fri.pbd2019.runsup.model;

import android.location.Location;

import com.j256.ormlite.field.DatabaseField;

import java.util.Date;


public class GpsPoint {

    public GpsPoint () {
        this.created = new Date();
        this.lastUpdate = new Date();
    }

    public GpsPoint (Workout workout, long sessionNumber, Location location, long duration, float speed, double pace, double totalCalories) {
        this.workout = workout;
        this.sessionNumber = sessionNumber;
        if (location != null ) {
            this.latitude = location.getLatitude();
            this.longitude = location.getLongitude();
        }
        this.totalCalories = totalCalories;
        this.speed = speed;
        this.duration = duration;
        this.pace = pace;
        this.created = new Date();
        this.lastUpdate = new Date();
    }

    @DatabaseField(generatedId = true)
    private long id;

    @DatabaseField(foreign = true)
    private Workout workout;

    @DatabaseField
    private long sessionNumber;

    @DatabaseField
    private double latitude;

    @DatabaseField
    private double longitude;

    @DatabaseField
    private long duration;

    @DatabaseField
    private float speed;

    @DatabaseField
    private double pace;

    @DatabaseField
    private double totalCalories;

    @DatabaseField
    private Date created;

    @DatabaseField
    private Date lastUpdate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Workout getWorkout() {
        return workout;
    }

    public void setWorkout(Workout workout) {
        this.workout = workout;
    }

    public long getSessionNumber() {
        return sessionNumber;
    }

    public void setSessionNumber(long sessionNumber) {
        this.sessionNumber = sessionNumber;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public double getPace() {
        return pace;
    }

    public void setPace(double pace) {
        this.pace = pace;
    }

    public double getTotalCalories() {
        return totalCalories;
    }

    public void setTotalCalories(double totalCalories) {
        this.totalCalories = totalCalories;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return "GpsPoint{" +
                "id=" + id +
                ", workout=" + workout +
                ", sessionNumber=" + sessionNumber +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", duration=" + duration +
                ", speed=" + speed +
                ", pace=" + pace +
                ", totalCalories=" + totalCalories +
                ", created=" + created +
                ", lastUpdate=" + lastUpdate +
                '}';
    }
}
