package si.uni_lj.fri.pbd2019.runsup;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.io.FileNotFoundException;
import java.sql.SQLException;

import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.UserProfile;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

import static android.widget.Toast.LENGTH_LONG;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    //DATABASE
    DatabaseHelper dbHelper;
    Dao<User,Long> userDao;
    Dao<UserProfile,Long> userProfileDao;

    //USER(PROFILE) CREDENTIALS
    private String accId;
    private String authToken;
    private int weight;

    private static final int RC_SIGN_IN = 3;
    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;

    public static Uri slika;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Login");

        GoogleSignInOptions gso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.default_web_client_id))
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();

        findViewById(R.id.sign_in_button).setOnClickListener(this);
        findViewById(R.id.sign_out_button).setOnClickListener(this);

        initDB();
    }

    private void initDB () {
        dbHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try {
            userDao = dbHelper.userDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            userProfileDao = dbHelper.userProfileDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void novUser (int weight) {
        User novVnos = new User(accId);
        try {
            novVnos.setAuthToken(authToken);
            userDao.create(novVnos);
        } catch (SQLException e) {
            Log.d("kurko", "nov User NI kreiran!");
            e.printStackTrace();
        }
        UserProfile novVnosProfile = new UserProfile();
        try {
            if (weight == -1) {
                novVnosProfile.setWeight(60);
            } else {
                novVnosProfile.setWeight(weight);
            }
            novVnosProfile.setUser(novVnos);
            userProfileDao.create(novVnosProfile);
        } catch (SQLException e) {
            Log.d("kurko", "nov User NI kreiran!");
            e.printStackTrace();
        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        // Firebase sign out
        mAuth.signOut();

        // Google sign out
        mGoogleSignInClient.signOut().addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Context context = LoginActivity.this;
                SharedPreferences prefs = context.getSharedPreferences(
                        "runsup", Context.MODE_PRIVATE);
                prefs.edit().putBoolean("userSignedIn", false).apply();
                prefs.edit().remove("userId").apply();
                prefs.edit().remove("personName").apply();
                prefs.edit().remove("personEmail").apply();
                prefs.edit().remove("personPhotoUrl").apply();

                Toast.makeText(context, "User successfully logged out!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.sign_in_button) {
            signIn();
        } else if (i == R.id.sign_out_button) {
            signOut();
        }
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success
                            Log.d("kurko", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            accId = acct.getId();
                            authToken = acct.getIdToken();
                            String personName = acct.getDisplayName();
                            String personEmail = acct.getEmail();
                            String personPhotoUrl = "";
                            if (acct.getPhotoUrl() != null) {
                                slika = acct.getPhotoUrl();
                                personPhotoUrl = acct.getPhotoUrl().toString();
                            } else {
                                slika = null;
                            }

                            EditText weightValue =  (EditText) findViewById(R.id.weight_value);
                            String content = weightValue.getText().toString();
                            int weight = -1;
                            if (content.length() > 0) {
                                weight = Integer.valueOf(content);
                            }
                            novUser(weight);

                            Context context = LoginActivity.this;
                            SharedPreferences prefs = context.getSharedPreferences(
                                    "runsup", Context.MODE_PRIVATE);

                            prefs.edit().putBoolean("userSignedIn", true).apply();
                            prefs.edit().putString("userId", accId).apply();
                            prefs.edit().putString("personName", personName).apply();
                            prefs.edit().putString("personEmail", personEmail).apply();
                            prefs.edit().putString("personPhotoUrl", personPhotoUrl).apply();

                            Toast.makeText(context, "User successfully logged in!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, MainActivity.class);
                            startActivity(intent);
                        } else {
                            Log.w("kurko", "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Login failed!", Toast.LENGTH_LONG).show();
                            //TODO updateUI(null);
                        }
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                Log.w("kurko", "Google sign in failed", e);
            }
        }
    }


}
