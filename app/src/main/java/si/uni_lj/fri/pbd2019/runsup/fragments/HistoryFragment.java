package si.uni_lj.fri.pbd2019.runsup.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.HistoryListAdapter;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {

    View v;
    List<Workout> allWorkouts = new ArrayList<>();

    //DATABASE
    DatabaseHelper dbHelper;
    Dao<Workout,Long> workoutDao;

    public HistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("History");
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_history, container, false);
        initDB();
        try {
            allWorkouts = workoutDao.queryForEq("ended", true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ListView mListView = (ListView) v.findViewById(R.id.listview_history_workouts);
        TextView mTextView = (TextView) v.findViewById(R.id.textview_history_noHistoryData);

        if (allWorkouts == null || allWorkouts.size() == 0) {
            mListView.setVisibility(View.GONE);
        } else {
            mTextView.setVisibility(View.GONE);
            HistoryListAdapter adapter = new HistoryListAdapter(getActivity(), R.layout.adapter_history, allWorkouts);
            mListView.setAdapter(adapter);
        }
        return v;
    }
    private void initDB () {
        dbHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        try {
            workoutDao = dbHelper.workoutDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
