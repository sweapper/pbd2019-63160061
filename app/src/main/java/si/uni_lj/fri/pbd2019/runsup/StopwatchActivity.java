package si.uni_lj.fri.pbd2019.runsup;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.stetho.Stetho;
import com.google.android.gms.tasks.OnSuccessListener;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;

public class StopwatchActivity extends AppCompatActivity {
    private static final String TAG = StopwatchActivity.class.getSimpleName();

    //@BindView(R.id.button_stopwatch_start) Button startButton;
    //Button endWorkoutButton = (Button)findViewById(R.id.button_stopwatch_endworkout);
    //@BindView(R.id.button_stopwatch_endworkout) Button endWorkoutButton;
    private BroadcastReceiver mBroadcastReceiver;
    static final int REQUEST_ID_LOCATION_PERMISSIONS = 0;

    static final int STATE_STOPPED = 0;
    static final int STATE_RUNNING = 1;
    static final int STATE_PAUSED = 2;
    static final int STATE_CONTINUE = 3;

    private int noLocChangeCounter = 0;

    private int sessionNumber = 0;
    private float speed;
    private long workoutId;

    private int state;
    private int sportActivity = 0;
    private long duration = 0;
    private double distance = 0.0;
    private double calories = 0.0;
    private double pace = 0.0;
    private ArrayList<List<Location>> finalPositionList = new ArrayList<List<Location>>();
    private ArrayList<Location> positionList = new ArrayList<Location>();
    private Location lastLocation = null;

    //DATABASE
    DatabaseHelper dbHelper;
    Dao<GpsPoint,Long> gpsPointDao;
    Dao<Workout,Long> workoutDao;

    //TODO bundle saveinstance state - da se podatki ne uničjo v onCreate an onDestroy
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stopwatch);

        initDB();

        mBroadcastReceiver = new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                state = intent.getIntExtra("state", -1);
//                if (state == 0) {
//                    Log.d("kurko", "STOPPED");
//                } else if (state == 1) {
//                    Log.d("kurko", "RUNNING");
//                } else if (state == 2) {
//                    Log.d("kurko", "PAUSED");
//                } else if (state == 3) {
//                    Log.d("kurko", "CONTINUE");
//                }
                if (state == 2) {
                    finalPositionList.add(positionList);
                    positionList = new ArrayList<Location>();
                } else {
                    pace = intent.getDoubleExtra("pace", -1);
                    duration = intent.getLongExtra("duration", -1);
                    distance = intent.getDoubleExtra("distance", -1);
                    calories = intent.getDoubleExtra("calories", -1);
                    speed = intent.getFloatExtra("speed", -1);
                    workoutId = intent.getLongExtra("workoutId", -1);

                    Bundle bun = intent.getExtras();
                    // ce je nova lokacija jo dodamo v list
                    if (bun != null) {
                        Location loc = (Location)bun.get("location");
                        // ce nu null se posodobi lokacija
                        if (loc != null) {
                            positionList.add(loc);
                            lastLocation = loc;
                            noLocChangeCounter = 0;
                            //novGpsPoint(loc);
                        } else {
                            noLocChangeCounter++;
                            if (noLocChangeCounter >= 10) {
                                noLocChangeCounter = 0;
                                //novGpsPoint(lastLocation);
                            }
                        }
                    }

                    TextView dur = (TextView) findViewById(R.id.textview_stopwatch_duration);
                    dur.setText(MainHelper.formatDuration(duration/1000));

                    TextView dist = (TextView) findViewById(R.id.textview_stopwatch_distance);
                    dist.setText(MainHelper.formatDistance(distance));

                    TextView cal = (TextView) findViewById(R.id.textview_stopwatch_calories);
                    cal.setText(MainHelper.formatCalories(calories));

                    TextView pac = (TextView) findViewById(R.id.textview_stopwatch_pace);
                    pac.setText(MainHelper.formatPace(pace));
                }
            }
        };

        askLocationPermission();

        Intent intent = new	Intent(this, TrackerService.class);
        startService(intent);
    }

    private void initDB () {
        dbHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try {
            gpsPointDao = dbHelper.gpsPointDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            workoutDao = dbHelper.workoutDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void askLocationPermission() {
        Snackbar.make(findViewById(R.id.constraintlayout_stopwatch_content), R.string.location,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ActivityCompat.requestPermissions(StopwatchActivity.this,
                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                        Manifest.permission.ACCESS_FINE_LOCATION},
                                REQUEST_ID_LOCATION_PERMISSIONS);
                    }
                })
                .show();
    }

    public void onButtonSelectSportPressed(View view) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setIcon(R.drawable.ic_directions_run_black_24dp);
        builderSingle.setTitle("Select Sport Activity");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
        for (int activity = 0; activity < 3; activity++) {
            arrayAdapter.add(SportActivities.getSportActivityName(activity));
        }
        builderSingle.setNegativeButton(getResources().getString(R.string.cancel), null);
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialog, int which) {
                changeSportActivity(which);
                Intent intent = new	Intent(StopwatchActivity.this, TrackerService.class);
                intent.putExtra("sportActivity", sportActivity);
                intent.setAction("si.uni_lj.fri.pdb2019.runsup.UPDATE_SPORT_ACTIVITY");
                startService(intent);
            }
        });
        builderSingle.show();
    }

    public void changeSportActivity (int activity) {
        this.sportActivity = activity;
    }

    public void startActive (View view) {
        Intent intent = new	Intent(this, ActiveWorkoutMapActivity.class);
        intent.putExtra("workoutId", workoutId);
        startActivity(intent);
    }

    public void startStopWorkout (View view) {
        Button startButton = (Button) view;
        String textStart = startButton.getText().toString();
        View endWorkoutButton = findViewById(R.id.button_stopwatch_endworkout);

        if (textStart.equals("Start")) {
            sessionNumber = 0;
            Button btn = (Button) findViewById(R.id.button_stopwatch_selectsport);
            btn.setEnabled(false);
            startButton.setText(R.string.stopwatch_stop);
            Intent intent = new	Intent(this, TrackerService.class);
            intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_START");
            startService(intent);
        } else if (textStart.equals("Stop")) {
            startButton.setText(R.string.stopwatch_continue);
            endWorkoutButton.setVisibility(View.VISIBLE);
            Intent intent = new	Intent(this, TrackerService.class);
            intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE");
            startService(intent);
        } else if (textStart.equals("Continue")) {
            startButton.setText(R.string.stopwatch_stop);
            endWorkoutButton.setVisibility(View.GONE);
            sessionNumber++;
            Intent intent = new	Intent(this, TrackerService.class);
            intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_CONTINUE");
            startService(intent);
        }
    }

    public void endWorkout (View view) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.stopwatch_endworkout)
                .setIcon(R.drawable.ic_warning_black_24dp)
                .setMessage(R.string.end)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int a) {
                        /*TODO
                        Intent intenT = new	Intent(this, TrackerService.class);
                        intenT.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_STOP");
                        startService(intenT);
                        */
                        Button btn = (Button) findViewById(R.id.button_stopwatch_selectsport);
                        btn.setEnabled(true);
                        Intent intent = new Intent(StopwatchActivity.this, WorkoutDetailActivity.class);
                        intent.putExtra("sportActivity", sportActivity);
                        intent.putExtra("duration", duration);
                        intent.putExtra("distance", distance);
                        intent.putExtra("calories", calories);
                        intent.putExtra("workoutId", workoutId);
                        intent.putExtra("pace", pace);
                        intent.putExtra("finalPositionList", finalPositionList);
                        startActivity(intent);
                    }
                })
                .setNegativeButton(R.string.no, null)
                .show();

        Intent intent = new	Intent(this, TrackerService.class);
        intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_STOP");
        startService(intent);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (state == STATE_STOPPED) {
            Intent intent = new	Intent(this, TrackerService.class);
            startService(intent);
        }
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mBroadcastReceiver, new IntentFilter("si.uni_lj.fri.pbd2019.runsup.TICK"));
    }

    @Override
    public void onPause() {
        super.onPause();
        if (state == STATE_STOPPED) {
            Intent intent = new	Intent(this, TrackerService.class);
            stopService(intent);
        }
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mBroadcastReceiver);
    }

}