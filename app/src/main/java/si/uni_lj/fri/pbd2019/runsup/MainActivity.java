package si.uni_lj.fri.pbd2019.runsup;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.stetho.Stetho;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.fragments.AboutFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.HistoryFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.StopwatchFragment;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    //DATABASE
    DatabaseHelper dbHelper;
    Dao<Workout,Long> workoutDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Stetho.initializeWithDefaults(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        //TODO posodobi ce je user logiran
        ImageView slikaUserja = (ImageView) header.findViewById(R.id.menu_loggedInUserImage);
        TextView usernameUserja = (TextView) header.findViewById(R.id.menu_loggedInUserFullName);
        TextView emailUserja = (TextView) header.findViewById(R.id.menu_loggedInUserEmail);

        SharedPreferences prefs = MainActivity.this.getSharedPreferences(
                "runsup", Context.MODE_PRIVATE);
        Boolean jePrijavljen = prefs.getBoolean("userSignedIn", false);

        if (jePrijavljen) {
            //String id = prefs.getString("userId", "");
            String name = prefs.getString("personName", "");
            String email = prefs.getString("personEmail", "");
            usernameUserja.setText(name);
            emailUserja.setText(email);
            String personPhotoUrl = prefs.getString("personPhotoUrl", "");
            if (personPhotoUrl.length() > 0) {
                //TODO ne dela
//                if (LoginActivity.slika != null) {
//                    try {
//                        Bitmap bmp;
//                        bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(LoginActivity.slika));
//                        slikaUserja.setImageBitmap(bmp);
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    }
//                }
                slikaUserja.setImageURI(LoginActivity.slika);
            } else {
                slikaUserja.setImageResource(R.mipmap.ic_launcher_round);
            }
        } else {
            slikaUserja.setImageResource(R.mipmap.ic_launcher_round);
            usernameUserja.setText(getResources().getString(R.string.all_unknownuser));
            emailUserja.setText(getResources().getString(R.string.all_unknownemail));
        }
        initDB();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new StopwatchFragment()).commit();
    }

    private void initDB () {
        dbHelper = OpenHelperManager.getHelper(MainActivity.this, DatabaseHelper.class);
        try {
            workoutDao = dbHelper.workoutDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void prijava (View view) {
        //TODO treba je prevert cec ni noben use logiran
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        } else if (id == R.id.action_clear_history) {
            List<Workout> allWorkouts = new ArrayList<>();
            try {
                allWorkouts = workoutDao.queryForEq("ended", true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (allWorkouts.size() != 0) {
                for (Workout wor : allWorkouts) {
                    try {
                        workoutDao.delete(wor);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HistoryFragment()).commit();
        } else if (id == R.id.action_sync) {
            Toast.makeText(MainActivity.this, "sync", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    public void startAdvanture() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.start_advanture)
                .setIcon(R.drawable.ic_warning_black_24dp)
                .setMessage(R.string.clear_workout)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int a) {
                        Intent intent = new Intent(MainActivity.this, AdvantureActivity.class);
                        startActivity(intent);
                    }
                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        //TODO tukej je treba fragmente klicat
        if (id == R.id.nav_workout) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new StopwatchFragment()).commit();
        } else if (id == R.id.nav_history) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HistoryFragment()).commit();
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_about) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AboutFragment()).commit();
        } else if (id == R.id.nav_advanture) {
            startAdvanture();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
