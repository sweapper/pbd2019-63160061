package si.uni_lj.fri.pbd2019.runsup;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.fragments.StopwatchFragment;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class WorkoutDetailActivity extends AppCompatActivity {

    //DATABASE
    DatabaseHelper dbHelper;
    Dao<Workout,Long> workoutDao;

    Workout w;
    private double distance;
    private long duration;
    private long workoutId;
    private int sportActivity;
    ArrayList<List<Location>> finalPositionList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_detail);
        initDB();

        Intent intent = getIntent();
        sportActivity = intent.getIntExtra("sportActivity", -1);
        duration = intent.getLongExtra("duration", -1);
        duration = duration / 1000;
        distance = intent.getDoubleExtra("distance", -1);
        double calories = intent.getDoubleExtra("calories", -1);
        double pace = intent.getDoubleExtra("pace", -1);
        //TODO to lah tut berem iz baze s querryjem
        finalPositionList = (ArrayList<List<Location>>) getIntent().getSerializableExtra("finalPositionList");
        workoutId = intent.getLongExtra("workoutId", -1);

        List<Workout> lastWorkout = new ArrayList<>();
        try {
            lastWorkout = workoutDao.queryForEq("id", workoutId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Log.d("kurko", "dobil sem workout: " + String.valueOf(workoutId));
        w = lastWorkout.get(0);
        w.setEnded(true);
        SharedPreferences prefs = getSharedPreferences(
                "runsup", Context.MODE_PRIVATE);
        prefs.edit().putBoolean("startedWorkout", false).apply();

        String sportName = "Wrong Number";
        switch (w.getSportActivity()) {
            case (0):
                sportName = "Running";
                break;
            case (1):
                sportName = "Walking";
                break;
            case (2):
                sportName = "Cycling";
                break;
        }
        //TODO na koncu preberi kaj pise tuki in shrani v DB
        EditText title = (EditText) findViewById(R.id.textview_workoutdetail_workouttitle);
        title.setText(w.getTitle());

        TextView sport = (TextView) findViewById(R.id.textview_workoutdetail_sportactivity);
        sport.setText(sportName);

        String paceS = MainHelper.formatPace(w.getPaceAvg()) + " " + getResources().getString(R.string.all_min) + "/" + getResources().getString(R.string.all_labeldistanceunitkilometers);
        TextView pac = (TextView) findViewById(R.id.textview_workoutdetail_valueavgpace);
        pac.setText(paceS);

        TextView dur = (TextView) findViewById(R.id.textview_workoutdetail_valueduration);
        dur.setText(MainHelper.formatDuration(w.getDuration()/1000));

        String distanceS = MainHelper.formatDistance(w.getDistance()) + " " + getResources().getString(R.string.all_labeldistanceunitkilometers);
        TextView dist = (TextView) findViewById(R.id.textview_workoutdetail_valuedistance);
        dist.setText(distanceS);

        String caloriesS = MainHelper.formatCalories(w.getTotalCalories()) + " " + getResources().getString(R.string.all_labelcaloriesunit);
        TextView cal = (TextView) findViewById(R.id.textview_workoutdetail_valuecalories);
        cal.setText(caloriesS);

        DateFormat konecWorkouta = SimpleDateFormat.getDateTimeInstance();
        Date date = new Date();
        TextView workoutDate = (TextView) findViewById(R.id.textview_workoutdetail_activitydate);
        workoutDate.setText(konecWorkouta.format(date));
        try {
            workoutDao.update(w);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Intent intent2 = new Intent(WorkoutDetailActivity.this, TrackerService.class);
        intent2.setAction("si.uni_lj.fri.pdb2019.runsup.KILL");
        startService(intent2);
    }

    private void initDB () {
        dbHelper = OpenHelperManager.getHelper(WorkoutDetailActivity.this, DatabaseHelper.class);
        try {
            workoutDao = dbHelper.workoutDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.wd_menu, menu); //your file name
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_delete:
                try {
                    workoutDao.delete(w);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                android.os.Process.killProcess(android.os.Process.myPid());
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        EditText title = (EditText) findViewById(R.id.textview_workoutdetail_workouttitle);
        w.setTitle(title.getText().toString());
        try {
            workoutDao.update(w);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public void shareWorkout(View view) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String text = "I was out for " + SportActivities.getSportActivityName(sportActivity) + ". I did " +
                MainHelper.formatDistance(distance) + " " + R.string.all_labeldistanceunitkilometers + " in " + MainHelper.formatDuration(duration) + ".";
        intent.putExtra(Intent.EXTRA_TEXT, text);
        startActivity(Intent.createChooser(intent, "Share your workout!"));
    }

    public void showMap (View view) {
        Intent intent = new Intent(WorkoutDetailActivity.this, MapsActivity.class);
        intent.putExtra("workoutId", workoutId);
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
