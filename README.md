# Miha Ben�ina  
#### mb6315@student.uni-lj.si  

## The idea

The thing is that I am not the biggest fan of running (it's just not enough going on),
so I tried to figure out how I would make the app more entertaining. At the end I decided I was going to
gamify it. I named my RunsUp extension "Adventure time!".

## Adventure time!

To open the extension you can navigate to it through the MainActivity's drawer layout
(as shown on the picture below).

![Menu](screens/sprint4/menu.PNG)

The first thing you see is the front cover from which you can navigate to the high scores,
instructions page or the start button (which starts a new adventure).

![Main page](screens/sprint4/adventure_main_page.PNG)

When you start an adventure a Google Map will be loaded. There are 4 types of location pins that
are spawned by the game. The first one is the regular red one which shows where you started.
The blue one tracks your current location and the colorful ones are called checkpoints which
spawn on random locations near you and give you bonus points.

![Gameplay 1](screens/sprint4/playing_game.PNG)

There are also 2 "chasers" (the evil green Android figures). Your goal is to get as many points as
you can, before the chasers catch you. Your overall score increases with distance covered,
total time of the adventure and of course the checkpoints reached (that's where the big 
points come from). You can track your progress at the top of the screen. 
Upon reaching a checkpoint the player gets notified by a specific vibration
as well as a rewarding sound. When the checkpoint is reached it also despawns (as shown on the picture below).

![Gameplay 2](screens/sprint4/near_chaser.PNG)

As we can see on the picture above, the bottom chaser turned red. The reason is because he got
dangerously close to you (you also get notified by a vibration - so you don't have to look at the
screen all the time to know what's going on for the sake of safety). Upon reaching 3 
checkpoints new checkpoints spawn and the old ones despawn. Chaser's positions are also reset
to new random locations.  
When one of the chasers catches you it's game over!

![Game over](screens/sprint4/game_over.PNG)

You can than see how well you did in the high scores page. There are also 3 robots in the high scores
to keep you motivated to improve.

![High scores](screens/sprint4/high_scores.PNG)

## Conclusion

The main point of the game is to be fun to play so I really put a lot of work into programming
the robots so they are not too fast but not to slow to keep you entertained.  
I also took my time to make the game appealing to the user with animations and other frontend
components. I also tested the game and got rid of some annoying things that could potentially
frustrate the user (for example you don't need to come to the exact location of the checkpoint,
but near it because it is random so it can spawn at a spot you can't reach).  

Feel free to try it, it's really fun!